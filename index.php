<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="jquery.js"></script>
</head>
<body>
    <input type="text" name="firstname" id="fname" />
    <input type="button" value="My Btn" id="btn" />
    <div class="output"></div>
    <script>
        $(function(){
            $('#fname').keyup(function(){
                let firstname = $('#fname').val();
                $.ajax({
                    url:'getdata.php',
                    type:'post',
                    data:{fname:firstname},
                    success:function(data){
                        $('.output').html(data);
                    }
                })
            });
        });
    </script>
</body>
</html>